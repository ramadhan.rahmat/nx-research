import styled from 'styled-components';

import NxWelcome from './nx-welcome';
import ProductList from '../components/ProductList';

const StyledApp = styled.div`
  // Your style here
`;

export function App() {
  return (
    <StyledApp>
      <ProductList />
      <NxWelcome title="product-list" />
    </StyledApp>
  );
}

export default App;
