import {
  CardContainer,
  CardContent,
  CardImage,
  Description,
  Price,
  Title,
} from './styled';

interface ProductCardProps {
  image: string;
  title: string;
  description: string;
  price: number | string;
}

const ProductCard = ({
  image,
  title,
  description,
  price,
}: ProductCardProps) => {
  return (
    <CardContainer>
      <CardImage src={image} alt={title} />
      <CardContent>
        <Title>{title}</Title>
        <Description>{description}</Description>
        <Price>${price}</Price>
      </CardContent>
    </CardContainer>
  );
};

export default ProductCard;
