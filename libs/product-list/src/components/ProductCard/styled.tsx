import styled from 'styled-components';

// Styled Components
export const CardContainer = styled.div`
  width: 300px;
  border: 1px solid #ccc;
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

export const CardImage = styled.img`
  width: 100%;
  height: 200px;
  object-fit: cover;
`;

export const CardContent = styled.div`
  padding: 16px;
`;

export const Title = styled.h2`
  font-size: 1.2rem;
  margin-bottom: 8px;
`;

export const Description = styled.p`
  font-size: 1rem;
  color: #666;
`;

export const Price = styled.p`
  font-size: 1.2rem;
  font-weight: bold;
  margin-top: 8px;
`;
