import ProductCard from '../ProductCard';

const ProductList = () => {
  // Mock data
  const products = [
    {
      id: 1,
      title: 'Product 1',
      description: 'This is the first product',
      price: 19.99,
      image: 'https://via.placeholder.com/300',
    },
    {
      id: 2,
      title: 'Product 2',
      description: 'This is the second product',
      price: 29.99,
      image: 'https://via.placeholder.com/300',
    },
    {
      id: 3,
      title: 'Product 3',
      description: 'This is the third product',
      price: 39.99,
      image: 'https://via.placeholder.com/300',
    },
  ];

  return (
    <div>
      {products.map((product) => (
        <ProductCard
          key={product.id}
          title={product.title}
          description={product.description}
          price={product.price}
          image={product.image}
        />
      ))}
    </div>
  );
};

export default ProductList;
