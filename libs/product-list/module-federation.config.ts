import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'product-list',

  exposes: {
    './ProductList': './src/components/ProductList/index.tsx',
  },
};

export default config;
