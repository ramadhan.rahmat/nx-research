import * as React from 'react';

import NxWelcome from './nx-welcome';

import { Link, Route, Routes } from 'react-router-dom';

// @ts-ignore
const ProductList = React.lazy(() => import('product-list/ProductList'));

export function App() {
  return (
    <React.Suspense fallback={null}>
      <ProductList />
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
      </ul>
      <Routes>
        <Route path="/" element={<NxWelcome title="e-commerce" />} />
      </Routes>
    </React.Suspense>
  );
}

export default App;
